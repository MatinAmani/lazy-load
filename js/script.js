const cards = [...document.getElementsByClassName('card')]
const cardContainer = document.getElementById('card-container')

const observer = new IntersectionObserver(
    (entries) => {
        entries.forEach((entry) => {
            entry.target.classList.toggle('show', entry.isIntersecting)
        })
    },
    { threshold: 1 }
)

const lastObserver = new IntersectionObserver((entries) => {
    const lastCard = entries[0]
    if (!lastCard.isIntersecting) return
    loadNewCards()
    lastObserver.unobserve(lastCard.target)
    lastObserver.observe(document.querySelector('.card:last-child'))
})

cards.forEach((card) => observer.observe(card))
lastObserver.observe(document.querySelector('.card:last-child'))

const loadNewCards = () => {
    for (i = 0; i < 10; i++) {
        const card = document.createElement('div')
        card.textContent = 'New Loaded Card'
        card.classList.add('card')
        observer.observe(card)
        cardContainer.appendChild(card)
    }
}
